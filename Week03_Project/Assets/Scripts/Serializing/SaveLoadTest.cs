using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SaveLoadTest : MonoBehaviour
{
    public GameData gd;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            SaveDataManagement.Save(gd); //Change to SaveDataManager to do Json or SaveDataBinaryManager for Binary
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            gd = SaveDataManagement.Load();
        }
    }
}

[CustomEditor(typeof(SaveLoadTest))]
public class SaveAndLoad : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        SaveLoadTest saveUI = (SaveLoadTest)target;
        if (GUILayout.Button("Serialize to Binary Data"))
        {
            SaveDataBinaryManager.Save(saveUI.gd);
        }

        if (GUILayout.Button("Serialize to Json Data"))
        {
            SaveDataManagement.Save(saveUI.gd);
        }

        if (GUILayout.Button("Load Binary Data"))
        {
            saveUI.gd = SaveDataBinaryManager.Load();
        }

        if (GUILayout.Button("Load Json Data"))
        {
            saveUI.gd = SaveDataManagement.Load();
        }
    }
}