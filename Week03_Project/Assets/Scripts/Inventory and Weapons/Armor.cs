using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Armor
{
    public string armorName;
    public int type;
    public int material;
    public int level;

}
