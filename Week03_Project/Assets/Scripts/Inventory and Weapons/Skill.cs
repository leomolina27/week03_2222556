using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Skill
{
    public string skillName;
    public int changeInSpeed;
    public int changeInDamage;
    public int changeInHealth;

}