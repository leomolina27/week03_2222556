using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Inventory : MonoBehaviour
{
    public List<Weapon> weapons = new List<Weapon>();
    public int itemSlots;
    [SerializeField]
    private GameObject bagReference;
    [SerializeField]
    private int startingItemSlots;

    [SerializeField]
    private Vector3 position;

    private void Update()
    {
        if (bagReference != null) position = bagReference.transform.position;
    }

}
